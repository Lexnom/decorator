def decorator_kvadrat(fun):
    def decot_test(x=10):
        res = fun(x)
        return res
    return decot_test

def check_type(fun):
    def check_str(x):
        if type(x) == str:
            return 'Сорябумаба не надо пихать строку'
        else:
            return fun(x)
    return check_str

@check_type
@decorator_kvadrat
def kvadrat(x):
    x *= x
    return x

if __name__ == '__main__':
    print(kvadrat('12'))
